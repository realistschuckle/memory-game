var HIDDEN = 0;
var SHOWN = 1;
var MATCHED = 2;

function initalizeGameState(number) {
	var cardStates = [];
	var values = [];
	for (var i = 0; i < 2 * number; i += 1) {
		cardStates.push(HIDDEN);
		values.push(Math.floor(i / 2));
	}
	values.sort(function () { return Math.random() - 0.5 });
	return {
		cardStates: cardStates,
		values: values,
	};
}

function printMessage(message, text) {
	message.innerHTML = text;
}

function updateBoard(board, state) {
	for (var i = 0; i < state.cardStates.length; i += 1) {
		var cardState = state.cardStates[i];
		var card = document.getElementById('card-' + i);
		switch (cardState) {
			case HIDDEN: {
				card.innerHTML = '';
				card.classList.remove('is-shown');
				card.classList.remove('is-matched');
				break;
			}
			case SHOWN: {
				card.innerHTML = state.values[i];
				card.classList.remove('is-matched');
				card.classList.add('is-shown');
				break;
			}
			case MATCHED: {
				card.innerHTML = state.values[i];
				card.classList.remove('is-shown');
				card.classList.add('is-matched');
				break;
			}
		}
	}
}

function isWinner(state) {
	for (var i = 0; i < state.cardStates.length; i += 1) {
		if (state.cardStates[i] !== MATCHED) {
			return false;
		}
	}
	return true;
}

function setupBoard(message, template, board, state) {
	board.innerHTML = '';
	for (var i = 0; i < state.cardStates.length; i += 1) {
		var clone = template.content.cloneNode(true);
		var card = clone.querySelector('.card');
		card.id = 'card-' + i;
		card.dataset.index = i;
		board.appendChild(card);
		card.addEventListener('click', function () {
			if (state.showing || state.won) {
				return;
			}

			var index = this.dataset.index;
			if (state.cardStates[index] === HIDDEN) {
				state.cardStates[index] = SHOWN;
			}
			updateBoard(board, state);

			var shownCardIndices = [];
			for (var i = 0; i < state.cardStates.length; i += 1) {
				if (state.cardStates[i] === SHOWN) {
					shownCardIndices.push(i);
				}
			}
			if (shownCardIndices.length === 2) {
				var first = shownCardIndices[0];
				var second = shownCardIndices[1];
				if (state.values[first] === state.values[second]) {
					state.cardStates[first] = MATCHED;
					state.cardStates[second] = MATCHED;
					updateBoard(board, state);
					if (isWinner(state)) {
						state.won = true;
						printMessage(message, "YOU WON!");
					} else {
						printMessage(message, 'You made a match! Great job!');
					}
				} else {
					state.showing = true;
					printMessage(message, 'Not a match. Try again.');
					setTimeout(function () {
						for (var i = 0; i < state.cardStates.length; i += 1) {
							if (state.cardStates[i] === SHOWN) {
								state.cardStates[i] = HIDDEN;
							}
						}
						state.showing = false;
						updateBoard(board, state);
					}, 1000);
				}
			}
		});
	}
	printMessage(message, 'Choose a card and try to match it.');
}

window.addEventListener('DOMContentLoaded', function () {
	var message = document.getElementById('message');
	var board = document.getElementById('board');
	var cardTemplate = document.getElementById('card-template');
	var number = document.getElementById('number');
	var newGame = document.getElementById('new-game');
	var currentGame = null;

	newGame.addEventListener('click', function () {
		var maxCardValue = parseInt(number.value);
		currentGame = initalizeGameState(maxCardValue);
		setupBoard(message, cardTemplate, board, currentGame);
	});
});

